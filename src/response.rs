use crate::word::Word;

use serde::Serialize;
use simple_websockets::Message;

#[derive(Serialize)]
pub struct MessageResponse {
    event: String,
    data: MessageResponseData,
}

impl MessageResponse {
    pub fn to_message(&self) -> Message {
        Message::Text(serde_json::to_string(&self).unwrap())
    }
}

#[derive(Serialize)]
#[serde(untagged)]
pub enum MessageResponseData {
    Greeting {
        id: u64,
        next_mora: Option<String>,
    },
    Word {
        author: u64,
        word: Word,
        next_mora: String,
    },
    History {
        words: Vec<Word>,
    },
    PlayerCount {
        players: u64,
    },
    Error {
        message: String,
    },
}

impl MessageResponseData {
    pub fn get_name(&self) -> String {
        String::from(match self {
            Self::Greeting { .. } => "greeting",
            Self::Word { .. } => "word",
            Self::History { .. } => "history",
            Self::PlayerCount { .. } => "playerCount",
            Self::Error { .. } => "error",
        })
    }

    pub fn into_response(self) -> MessageResponse {
        MessageResponse {
            event: self.get_name(),
            data: self,
        }
    }

    pub fn into_message(self) -> Message {
        self.into_response().to_message()
    }
}
