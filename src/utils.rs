use wana_kana::{ConvertJapanese, IsJapaneseStr};

pub fn get_starting_mora(word: &str) -> String {
    if word.is_empty() {
        return String::from("");
    }
    let word = word.to_hiragana();
    let mut iter = word.chars();
    let starting_char = iter.next().unwrap();
    let second_char = iter.next().unwrap();
    match second_char {
        'ゃ' | 'ゅ' | 'ょ' => format!("{starting_char}{second_char}"),
        _ => starting_char.to_string(),
    }
}

// Trim off lengtheners and non-kana that are irrelevant to shiritori
// TODO: Use slices
fn trim_irrelevant_chars(word: &str) -> String {
    let mut iter = word.chars().rev().peekable();
    while let Some(c) = iter.peek() {
        if *c == 'ー' || !c.to_string().is_kana() {
            iter.next();
        } else {
            break;
        }
    }
    iter.rev().collect()
}

// Get final mora, which could be multiple chars e.g. しゃ
// TODO: Use slices
pub fn get_final_mora(word: &str) -> String {
    let word = trim_irrelevant_chars(word).to_hiragana();
    if word.is_empty() {
        return String::from("");
    }
    let mut iter = word.chars().rev();
    let final_char = iter.next().unwrap();
    match final_char {
        'ゃ' | 'ゅ' | 'ょ' => format!(
            "{}{final_char}",
            match iter.next() {
                Some(c) => c.to_string(),
                None => String::from(""),
            }
        ),
        _ => final_char.to_string(),
    }
}
