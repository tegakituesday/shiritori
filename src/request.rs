use serde::Deserialize;

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub enum MessageRequest {
    Word {
        word: String,
    },
    ChangeRoom {
        name: String,
    },
}
