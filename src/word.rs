use chrono::{DateTime, Utc};
use jisho::Entry;
use serde::Serialize;
use std::convert::From;

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Word {
    #[serde(skip_serializing)]
    pub id: Option<i64>,
    pub word: String,
    pub reading: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub timestamp: Option<DateTime<Utc>>,
}

impl From<Entry> for Word {
    fn from(entry: Entry) -> Self {
        Self {
            id: None,
            word: entry.kanji,
            reading: entry.reading,
            timestamp: None,
        }
    }
}
