mod client;
mod database;
mod dictionary;
mod response;
mod request;
mod room;
mod server;
mod utils;
mod word;

use crate::server::{Server, ServerSettings};

fn main() {
    let settings = ServerSettings::default();
    let mut server = Server::new(&settings).unwrap_or_else(|error| {
        panic!(
            "Failed to start server at port {}: {:?}",
            settings.port, error
        )
    });
    server.run();
}
