use wana_kana::{ConvertJapanese, IsJapaneseStr};

fn raw_lookup(input: &str) -> Option<&jisho::Entry> {
    let input = input.trim();
    jisho::lookup(input).into_iter().find(|&word| {
        // If input has no kanji,
        // we can just compare the input to the reading verbatim
        // ensuring both are hiragana
        (input.is_kana() && input.to_hiragana() == word.reading.to_hiragana()) ||
        // Otherwise, we have to ensure that the input
        // is verbosely the same.
        // However, this will cause problems for some words.
        // For example, 照り焼き will be accepted but 照焼 won't.
        (input == word.kanji)
    })
}

pub fn lookup(query: &str) -> Option<jisho::Entry> {
    match raw_lookup(query) {
        Some(result) => Some(result.clone()),
        None => {
            if query.is_hiragana() {
                // looking up ごりら doesn't return ゴリラ
                // jisho::lookup for some reason refers to input string,
                // so cloning is required
                raw_lookup(&query.to_katakana()).cloned()
            } else if query.is_katakana() {
                // looking up シリトリ doesn't return 尻取り
                raw_lookup(&query.to_hiragana()).cloned()
            } else {
                None
            }
        }
    }
}
