ws = new WebSocket("ws://localhost:8080");
const div = document.querySelector("#shiritori");
const input = document.querySelector("#shiritori-input");
const roomInput = document.querySelector("#room-input");
const players = document.querySelector("#shiritori-players");
const iframe = document.querySelector("iframe");
let id = null;

function displayWord(_word, end, delay) {
  let { word, reading } = _word;
  p = document.createElement("p");
  p.innerHTML = `<a href="javascript:lookUpWord('${word || reading}')">${
    reading || word === reading ? (word === "" ? reading : `<ruby>${word}<rp>(</rp><rt>${reading}<rt/><rp>)</rp></ruby>`) : word
  }</a>`;
  if (delay != 0) {
    p.style.animationDelay = `${delay}s`;
  }
  if (end) {
    div.append(p);
  } else {
    div.prepend(p);
  }
  div.scrollTop = div.offsetHeight;
}

function updateInput(data) {
  if (data.next_mora !== null) {
    let waiting = data.author === id;
    input.placeholder = waiting ? "Waiting for other players..." : `${data.next_mora}…`;
    input.disabled = waiting;
    if (!waiting) input.focus();
  } else {
    input.placeholder = "";
    input.focus();
  }
}

function lookUpWord(word) {
  iframe.src = `https://jisho.org/search/${word}`;
}

ws.onmessage = e => {
  const { event, data } = JSON.parse(e.data);
  switch (event) {
    case "greeting":
      div.innerHTML = "";
      updateInput(data);
      if (typeof data.word !== "undefined") {
        lookUpWord(data.word.word);
      }
      break;
    case "word":
      displayWord(data.word, true, 0);
      updateInput(data);
      lookUpWord(data.word.word);
      break;
    case "history":
      for (let i = 0; i < data.words.length; i++) {
        displayWord(data.words[i], false, 0.1 * i);
      }
      break;
    case "playerCount":
      let otherPlayers = data.players - 1;
      players.innerHTML = `${otherPlayers === 0 ? "No" : otherPlayers} other player${otherPlayers === 1 ? "" : "s"} online.`;
      break;
    case "error":
      alert(data.message);
      break;
  }
}

input.addEventListener('keypress', e => {
  if (e.key === 'Enter') {
    ws.send(JSON.stringify({
      word: {
        word: input.value
      }
    }));
    input.value = "";
  }
});

function changeRoom(room) {
  ws.send(JSON.stringify({
    changeRoom: {
      name: room
    }
  }))
}

roomInput.addEventListener('keypress', e => {
  if (e.key === 'Enter') {
    changeRoom(roomInput.value);
    roomInput.value = "";
  }
 })
